resource "aws_s3_bucket" "example" {
  bucket = "s3-bucket-${random_id.server.hex}"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}

resource "random_id" "server" {
  byte_length = 8
}